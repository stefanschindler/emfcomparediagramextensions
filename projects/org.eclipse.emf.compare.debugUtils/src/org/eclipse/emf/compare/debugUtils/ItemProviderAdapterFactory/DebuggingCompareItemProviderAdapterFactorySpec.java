/*******************************************************************************
 * Copyright (c) 2012, 2015, 2018 Obeo and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Obeo - initial API and implementation
 *     Stefan Schindler - debugging adaptions
 *******************************************************************************/
package org.eclipse.emf.compare.debugUtils.ItemProviderAdapterFactory;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.compare.provider.spec.CompareItemProviderAdapterFactorySpec;
import org.eclipse.emf.compare.provider.spec.MatchItemProviderSpec;


public class DebuggingCompareItemProviderAdapterFactorySpec extends CompareItemProviderAdapterFactorySpec {

	/**
	 * Constructor calling super {@link #CompareItemProviderAdapterFactory()}.
	 */
	public DebuggingCompareItemProviderAdapterFactorySpec() {
		super();
	}

	/*
	@Override
	public Adapter createMatchAdapter() {
		return new DebuggingMatchItemProviderSpec(this);
	}
	*/
	
	@Override
	public Adapter createReferenceChangeAdapter() {
		return new DebuggingReferenceChangeItemProviderSpec(this);
	}

}
	