/*******************************************************************************
 * Copyright (c) 2012, 2014 Obeo.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.emf.compare.debugUtils.postprocessor;


import org.eclipse.emf.common.util.Monitor;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.debugUtils.console.EMFCompareDebugPrettyPrinter;
import org.eclipse.emf.compare.postprocessor.IPostProcessor;

import com.google.common.collect.Iterables;

/**
 * Post-processor to create the UML difference extensions.
 * 
 * @author <a href="mailto:cedric.notot@obeo.fr">Cedric Notot</a>
 */
public class LastPostProcessor implements IPostProcessor {

	public void postMatch(Comparison comparison, Monitor monitor) {
		
		System.out.println("LastPostProcessor.postMatch");		
	}

	public void postDiff(Comparison comparison, Monitor monitor) {
		
		System.out.println("LastPostProcessor.postDiff");
		printCount(comparison);
	}

	public void postRequirements(Comparison comparison, Monitor monitor) {
		
		System.out.println("LastPostProcessor.postRequirements");		
	}

	public void postEquivalences(Comparison comparison, Monitor monitor) {
		
		System.out.println("LastPostProcessor.postEquivalences");
		
	}

	public void postConflicts(Comparison comparison, Monitor monitor) {
		
		System.out.println("LastPostProcessor.postConflicts");		
	}

	public void postComparison(Comparison comparison, Monitor monitor) {
		
		System.out.println("LastPostProcessor.postComparison");
		
		printCount(comparison);
		
		EMFCompareDebugPrettyPrinter.printComparison(comparison, System.out);
		
		System.out.println("LPP END");
		
	}
	
	private void printCount(Comparison comp) {
		
		int size = Iterables.size(comp.getDifferences());
		
		System.out.println("diff count: " + size);
	}

}
