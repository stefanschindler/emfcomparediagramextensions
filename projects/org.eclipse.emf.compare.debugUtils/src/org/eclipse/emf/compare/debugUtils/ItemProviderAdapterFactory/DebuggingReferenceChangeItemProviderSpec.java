/*******************************************************************************
 * Copyright (c) 2012, 2016, 2018 Obeo and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Obeo - initial API and implementation
 *     Stefan Schindler - debugging adaptions
 *******************************************************************************/

package org.eclipse.emf.compare.debugUtils.ItemProviderAdapterFactory;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.provider.spec.ReferenceChangeItemProviderSpec;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.View;

public class DebuggingReferenceChangeItemProviderSpec extends ReferenceChangeItemProviderSpec {

	public DebuggingReferenceChangeItemProviderSpec(AdapterFactory adapterFactory) {
		
		super(adapterFactory);
	}
	
	@Override
	protected String getValueText(final ReferenceChange refChange) {
		
		String ret = super.getValueText(refChange);
		
		EObject eObj = refChange.getValue();
		
		if(!(eObj instanceof View)) {
			ret += ret.length() > 0?" (":"(";
			ret += eObj.getClass().getSimpleName();
			ret += "@" + Integer.toHexString(eObj.hashCode());
			ret += ")";
		}
		
		return ret;
	}

}
