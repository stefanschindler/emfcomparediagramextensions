/*******************************************************************************
* Copyright (c) 2013, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.debugUtils.filter;

import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.debugUtils.console.EMFCompareDebugPrettyPrinter;
import org.eclipse.emf.compare.rcp.ui.internal.structuremergeviewer.nodes.DiffNode;
import org.eclipse.emf.compare.rcp.ui.structuremergeviewer.filters.AbstractDifferenceFilter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edit.tree.TreeNode;

import org.eclipse.gmf.runtime.notation.Connector;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.Shape;

import com.google.common.base.Predicate;

public class GhostFilter extends AbstractDifferenceFilter {

	private static final Predicate<? super EObject> PREDICATE_WHEN_SELECTED = new Predicate<EObject>() {
		
		private int count = 0;
		
		@Override
		public boolean apply(EObject input) {
			
			boolean ret = false;
//			boolean tn = false;
//			boolean rc = false;
			
			if(count == 0) {
				//System.out.println("myGhostFilter(gf) first call!");
				System.out.println("myGhostFilter(gf) - list of input objects:");
			}
			count++;
			
			System.out.print("mgf(" + count + ") ");
			
			if (input instanceof TreeNode) {
				
				//System.out.println(count + " TreeNode");
//				tn = true;
				
				final EObject data = ((TreeNode) input).getData();
				
				if (data instanceof ReferenceChange) {
					
					//System.out.println(count + " ReferenceChange");
//					rc = true;
					
					final EReference reference = ((ReferenceChange) data).getReference();
					
					EReference pc = NotationPackage.eINSTANCE.getView_PersistedChildren(); 
					boolean pc_equals_ref = pc.equals(reference); 
					
					EReference pe = NotationPackage.eINSTANCE.getDiagram_PersistedEdges(); 
					boolean pe_equals_ref = pe.equals(reference);
					
					ret = pc_equals_ref || pe_equals_ref;
					
					//System.out.println("Ghostfilter: filter='" + ret + "'");
					
					
					
					if(ret){
						
						Diff diff = (Diff) ((DiffNode) input).getData();
						
						System.out.println(" ghost (" + data.getClass()  + " : " + data.toString() + ")");
						
						System.out.print("& ");
						
						EMFCompareDebugPrettyPrinter.printDiff(0, diff, System.out);
						
						final ReferenceChange refChange = (ReferenceChange) data;
						if(refChange.getValue() instanceof Shape) {
							
							System.out.println("Shape : " + ((Shape) refChange.getValue()).getType());
							
						} else if (refChange.getValue() instanceof Connector) {
							
							System.out.println("Connector : " + ((Connector) refChange.getValue()).getType());
						} else {
							
							System.out.println("FAIL");
						}
						
					} else {
						
						System.out.println(" noGhost (" + data.getClass()  + " : " + data.toString() + ")");
					}
					
				} else {
					System.out.println(" !rc (" + data.getClass()  + " : " + data.toString() + ")");
				}
				
			} else {
				System.out.println(" !tn (" + input.getClass()  + " : " + input.toString() + ")");
			}
			
			/*if(count == 0) {
				System.out.println("myGhostFilter - list of input objects:");
			}
			count++;
			
			StringBuilder out = new StringBuilder();
			out.append("(" + count + ") ");
			if(!ret) {
				out.append("f:f, ");
				out.append("tn:" + (tn?"t":"f") + ", ");
				out.append("rc:" + (rc?"t":"f") + ", ");
			} else {
				out.append("f:t, ");
			}
			out.append(input.getClass()  + " : " + input.toString());
			System.out.println(out);
			
			if(ret==true){
				Diff diff = (Diff) ((DiffNode) input).getData();
				EMFComparePrettyPrinter.printDiff(diff, System.out);
			}*/
			
			//ret=false;
			
			return ret;
		}
	};

	@Override
	public Predicate<? super EObject> getPredicateWhenSelected() {
		return PREDICATE_WHEN_SELECTED;
	}

}
