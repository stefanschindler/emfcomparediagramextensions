/*******************************************************************************
 * Copyright (c) 2012 Obeo and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Obeo - initial API and implementation
 *     Stefan Schindler - debugging adaptions
 *******************************************************************************/
package org.eclipse.emf.compare.debugUtils.console;

import com.google.common.collect.Iterables;

import java.io.PrintStream;
import java.util.Arrays;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Conflict;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.DifferenceKind;
import org.eclipse.emf.compare.DifferenceSource;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.MatchResource;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.diagram.internal.extensions.DiagramDiff;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.uml2.uml.NamedElement;

// TODO do we need to externalize this? For now, suppressing the NLS warnings.
/**
 * This class exposes methods to serialize a "human-readable" form of the comparison model onto a given
 * stream.
 * 
 * @author <a href="mailto:laurent.goubet@obeo.fr">Laurent Goubet</a>
 */
@SuppressWarnings("nls")
public class EMFCompareDebugPrettyPrinter {
	/** This is the max length of the columns we display for the Match. */
	private static final int COLUMN_LENGTH = 40;
	
	private static int diffCount = 0;

	/**
	 * Hides default constructor.
	 */
	private EMFCompareDebugPrettyPrinter() {
		// No need to construct an instance of this.
	}

	/**
	 * Prints the whole comparison on the given stream (might be {@code stream}).
	 * 
	 * @param comparison
	 *            The comparison we are to print on {@code stream}.
	 * @param stream
	 *            The {@link PrintStream} on which we should print this comparison model.
	 */
	public static void printComparison(Comparison comparison, PrintStream stream) {
		for (MatchResource res : comparison.getMatchedResources()) {
			stream.println("Matched resources :");
			stream.println("Left = " + res.getLeftURI());
			stream.println("Right = " + res.getRightURI());
			stream.println("origin = " + res.getOriginURI());
		}
		stream.println();

		printMatch(comparison, stream);

		stream.println();

		printDifferences(comparison, stream);
		
		stream.println();
		
		printMatches(comparison);
	}
	
	
	public static void printMatches(Comparison c) {
		
		diffCount = 0;
		EList<Match> matches = c.getMatches();
		for(int i=0; i<matches.size(); i++) {
			
			printMatch(i, matches.size(), 0, matches.get(i));
		}
		
	}
	
	public static void printMatch(int nr, int count, int level, Match m) {
		
		String origin = getMatchLevelHeading(level, m.getOrigin());
				
		String left = getMatchLevelHeading(level, m.getLeft());
		
		String right = getMatchLevelHeading(level, m.getRight());
		
		String diffs = " => " + m.getDifferences().size() + " diffs / " + m.getSubmatches().size() + " submatches";
				
		printLine(level, "level " + level + ", match " + ++nr + "/" + count + diffs);
		printLine(level, "origin(" + origin + ") : left(" + left + ") :  right(" + right + ")");
		
		System.out.println("");
		
		for(Diff diff : m.getDifferences()) {
			
			printDiff(level, diff);
			diffCount++;
		}
		
		System.out.println("");
		
		EList<Match> subMatches = m.getSubmatches();
		for(int i=0; i<subMatches.size(); i++) {
			
			Match subMatch = subMatches.get(i);
			printMatch(i, subMatches.size(), level+1, subMatch);
		}
	}
	
	public static void printDiff(int level, Diff diff) {
		
		String out = "";
		
		if (diff instanceof ReferenceChange) {
			ReferenceChange refChange = (ReferenceChange)diff;
			final String valueName;
			if (refChange.getValue() instanceof ENamedElement) {
				valueName = "(n) " + ((ENamedElement)refChange.getValue()).getName();
			} else {
				valueName = refChange.getValue().toString();
				//Object o = refChange.getValue();
				//valueName = "(tS) " + o.getClass().getSimpleName() + "@" + Integer.toHexString(o.getClass().hashCode());
			}
			String change = "";
			if (diff.getSource() == DifferenceSource.RIGHT) {
				change = "remotely ";
			}
			if (diff.getKind() == DifferenceKind.ADD) {
				change += "added to";
			} else if (diff.getKind() == DifferenceKind.DELETE) {
				change += "deleted from";
			} else if (diff.getKind() == DifferenceKind.CHANGE) {
				change += "changed from";
			} else {
				change += "moved from";
			}
//			final String objectName;
//			if (refChange.getMatch().getLeft() instanceof ENamedElement) {
//				objectName = ((ENamedElement)refChange.getMatch().getLeft()).getName();
//			} else if (refChange.getMatch().getRight() instanceof ENamedElement) {
//				objectName = ((ENamedElement)refChange.getMatch().getRight()).getName();
//			} else if (refChange.getMatch().getOrigin() instanceof ENamedElement) {
//				objectName = ((ENamedElement)refChange.getMatch().getOrigin()).getName();
//			} else {
//				objectName = "-";
//			}
			
			out = "[" + diffCount + "] (R) " + valueName + " has been " + change + " reference "
					+ refChange.getReference().getName();
			
			checkDiagramAndChildren(diff);
			
		} else if (diff instanceof AttributeChange) {
			final AttributeChange attChange = (AttributeChange)diff;
			String valueName = "null";
			if (attChange.getValue() != null) {
				valueName = attChange.getValue().toString();
			}
			String change = "";
			if (diff.getSource() == DifferenceSource.RIGHT) {
				change = "remotely ";
			}
			if (diff.getKind() == DifferenceKind.ADD) {
				change += "added to";
			} else if (diff.getKind() == DifferenceKind.DELETE) {
				change += "deleted from";
			} else if (diff.getKind() == DifferenceKind.CHANGE) {
				change += "changed from";
			} else {
				change += "moved from";
			}
			final String objectName;
			if (attChange.getMatch().getLeft() instanceof ENamedElement) {
				objectName = ((ENamedElement)attChange.getMatch().getLeft()).getName();
			} else if (attChange.getMatch().getRight() instanceof ENamedElement) {
				objectName = ((ENamedElement)attChange.getMatch().getRight()).getName();
			} else if (attChange.getMatch().getOrigin() instanceof ENamedElement) {
				objectName = ((ENamedElement)attChange.getMatch().getOrigin()).getName();
			} else {
				objectName = "";
			}
			out = "[" + diffCount + "] (A) value " + valueName + " has been " + change + " attribute "
					+ attChange.getAttribute().getName() + " of object " + objectName;
			
		} else if (diff instanceof DiagramDiff) {
			
			DiagramDiff ddiff = (DiagramDiff) diff;
			out = "[" + diffCount + "] (D) " + ddiff.toString() + " : " + ddiff.getView();
			
			checkDiagramAndChildren(ddiff);
			
		} else {
			out = "[" + diffCount + "] (ELSE) " + diff.toString();
		}
		
		printLine(level, out);
	}
	
	private static void checkDiagramAndChildren(EObject eo) {
		
		View view = null;
		if(eo instanceof DiagramDiff) {
			
			view = (View) ((DiagramDiff)eo).getView();
			
		} else if(eo instanceof ReferenceChange) {
			
			EObject value = ((ReferenceChange) eo).getValue();
			if(value instanceof View) {
				
				view = (View) ((ReferenceChange)eo).getValue();
				
			}			
		}
		if(view != null) {
			
			EObject cont = view.eContainer();
			EObject contFeat = view.eContainingFeature();
			TreeIterator<EObject> allContents = view.eAllContents();
			EReference ref = view.eContainmentFeature();
			EList<EObject> contents = view.eContents();
			EList<EObject> cRefs = view.eCrossReferences();
			
			Diagram d = view.getDiagram();
			EList<View> children = view.getChildren();
			
			System.nanoTime();
			
		}		
	}
	
	private static String getMatchLevelHeading(int level, EObject eo) {
		
		String ret = "-";
		if(eo != null) {
			ret = getSimpleClassName(eo);
			
			String name = getName(eo);
			ret += name != null && name.length() > 0 ? " " + name : "";
		}
		
		return ret;
	}
	
	private static String getSimpleClassName(EObject eo) {
		
		return eo.getClass().getSimpleName() + "@" + Integer.toHexString(eo.hashCode());
		
	}
	
	private static String getName(EObject eo) {
		
		String name = "";
		if(eo instanceof Diagram) {
			name = ((Diagram) eo).getName();
		}else if (eo instanceof NamedElement) {
			name = ((NamedElement) eo).getName();
		}
		
		return name;
	}
	
	private static void printLine(int level, String output) {
		
		
		System.out.println(String.format("%1$" + (level*2 + output.length()) + "s", output));
		
	}
	

	/**
	 * Prints all the Match elements contained by the given {@code comparison}. Each Match will be displayed
	 * on its own line.
	 * <p>
	 * For example, if the left model has two packages "package1" and "package2", but the right has "package1"
	 * and "package3", what we will display here depends on the Match : if "left.package1" is matched with
	 * "right.package1", but "package2" and "package3" did not match, this will print <code><pre>
	 * | package1 | package1 |
	 * | package2 |          |
	 * |          | package3 |
	 * </pre></code> On the contrary, if "package2" and "package3" did match, we will display <code><pre>
	 * | package1 | package1 |
	 * | package2 | package3 |
	 * </pre></code>
	 * </p>
	 * 
	 * @param comparison
	 *            The comparison which Matched elements we are to print on {@code stream}.
	 * @param stream
	 *            The {@link PrintStream} on which we should print the matched elements of this comparison.
	 */
	public static void printMatch(Comparison comparison, PrintStream stream) {
		final String separator = "+----------------------------------------+----------------------------------------+----------------------------------------+";
		final String leftLabel = "Left";
		final String rightLabel = "Right";
		final String originLabel = "Origin";
		stream.println(separator);
		stream.println('|' + formatHeader(leftLabel) + '|' + formatHeader(rightLabel) + '|'
				+ formatHeader(originLabel) + '|');
		stream.println(separator);
		for (Match match : comparison.getMatches()) {
			printMatch(match, stream);
		}
		stream.println(separator);
	}

	/**
	 * Prints all differences detected for the given {@code comparison} on the given {@code stream}.
	 * 
	 * @param comparison
	 *            The comparison which differences we are to print on {@code stream}.
	 * @param stream
	 *            The {@link PrintStream} on which we should print these differences.
	 */
	public static void printDifferences(Comparison comparison, PrintStream stream) {
		
		final Iterable<Diff> diffs = comparison.getDifferences();

		stream.println("ALL DIFFS"
				+ (Iterables.size(diffs) > 0 ? " (" + Iterables.size(diffs) + ")" : ""));
		int i=0;
		for (Diff diff : diffs) {
			printDiff(i++, diff, stream);
		}
		stream.println();
		
		
		/*
		final Iterable<ReferenceChange> refChanges = Iterables.filter(comparison.getDifferences(),
				ReferenceChange.class);

		stream.println("REFERENCE CHANGES"
				+ (Iterables.size(refChanges) > 0 ? " (" + Iterables.size(refChanges) + ")" : ""));
		i=0;
		for (Diff diff : refChanges) {
			printDiff(i++, diff, stream);
		}
		stream.println();

		*/

		final Iterable<AttributeChange> attChanges = Iterables.filter(comparison.getDifferences(),
				AttributeChange.class);
		stream.println("ATTRIBUTE CHANGES"
				+ (Iterables.size(attChanges) > 0 ? " (" + Iterables.size(attChanges) + ")" : ""));
		i=0;
		for (Diff diff : attChanges) {
			printDiff(i++, diff, stream);
		}
		stream.println();

		Iterable<Conflict> conflicts = comparison.getConflicts();
		stream.println(
				"CONFLICTS" + (Iterables.size(conflicts) > 0 ? " (" + Iterables.size(conflicts) + ")" : ""));
		for (Conflict conflict : conflicts) {
			printConflict(conflict, stream);
		}
		stream.println();
	}

	/**
	 * Prints the given {@link Conflict} on the given {@code stream}.
	 * 
	 * @param conflict
	 *            The conflict we need to print on {@code stream}.
	 * @param stream
	 *            The {@link PrintStream} on which we should print this conflict.
	 */
	private static void printConflict(Conflict conflict, PrintStream stream) {
		stream.println(conflict.getKind() + " conflict:");
		final Iterable<ReferenceChange> refChanges = Iterables.filter(conflict.getDifferences(),
				ReferenceChange.class);
		int i=0;
		for (Diff diff : refChanges) {
			stream.print("\t");
			printDiff(i++, diff, stream);
		}
		final Iterable<AttributeChange> attChanges = Iterables.filter(conflict.getDifferences(),
				AttributeChange.class);
		i=0;
		for (Diff diff : attChanges) {
			stream.print("\t");
			printDiff(i++, diff, stream);
		}

	}

	/**
	 * Prints the given {@link Diff difference} on the given {@code stream}.
	 * 
	 * @param diff
	 *            The difference we are to print on {@code stream}.
	 * @param stream
	 *            The {@link PrintStream} on which we should print this difference.
	 */
	public static void printDiff(int count, Diff diff, PrintStream stream) {
		if (diff instanceof ReferenceChange) {
			ReferenceChange refChange = (ReferenceChange)diff;
			final String valueName;
			if (refChange.getValue() instanceof ENamedElement) {
				valueName = "(n) " + ((ENamedElement)refChange.getValue()).getName();
			} else {
				valueName = refChange.getValue().toString();
				//Object o = refChange.getValue();
				//valueName = "(tS) " + o.getClass().getSimpleName() + "@" + Integer.toHexString(o.getClass().hashCode());
			}
			String change = "";
			if (diff.getSource() == DifferenceSource.RIGHT) {
				change = "remotely ";
			}
			if (diff.getKind() == DifferenceKind.ADD) {
				change += "added to";
			} else if (diff.getKind() == DifferenceKind.DELETE) {
				change += "deleted from";
			} else if (diff.getKind() == DifferenceKind.CHANGE) {
				change += "changed from";
			} else {
				change += "moved from";
			}
//			final String objectName;
//			if (refChange.getMatch().getLeft() instanceof ENamedElement) {
//				objectName = ((ENamedElement)refChange.getMatch().getLeft()).getName();
//			} else if (refChange.getMatch().getRight() instanceof ENamedElement) {
//				objectName = ((ENamedElement)refChange.getMatch().getRight()).getName();
//			} else if (refChange.getMatch().getOrigin() instanceof ENamedElement) {
//				objectName = ((ENamedElement)refChange.getMatch().getOrigin()).getName();
//			} else {
//				objectName = "-";
//			}
			
			/*
			String oldName = refChange.getReference().getName();
			String newName = count+":"+oldName.replaceAll("[0-9]{1,2}:", "");
			refChange.getReference().setName(newName);
			*/
			
			stream.println("[" + count + "] (R) " + valueName + " has been " + change + " reference "
					+ refChange.getReference().getName());
			
			
		} else if (diff instanceof AttributeChange) {
			final AttributeChange attChange = (AttributeChange)diff;
			String valueName = "null";
			if (attChange.getValue() != null) {
				valueName = attChange.getValue().toString();
			}
			String change = "";
			if (diff.getSource() == DifferenceSource.RIGHT) {
				change = "remotely ";
			}
			if (diff.getKind() == DifferenceKind.ADD) {
				change += "added to";
			} else if (diff.getKind() == DifferenceKind.DELETE) {
				change += "deleted from";
			} else if (diff.getKind() == DifferenceKind.CHANGE) {
				change += "changed from";
			} else {
				change += "moved from";
			}
			final String objectName;
			if (attChange.getMatch().getLeft() instanceof ENamedElement) {
				objectName = ((ENamedElement)attChange.getMatch().getLeft()).getName();
			} else if (attChange.getMatch().getRight() instanceof ENamedElement) {
				objectName = ((ENamedElement)attChange.getMatch().getRight()).getName();
			} else if (attChange.getMatch().getOrigin() instanceof ENamedElement) {
				objectName = ((ENamedElement)attChange.getMatch().getOrigin()).getName();
			} else {
				objectName = "";
			}
			stream.println("[" + count + "] (A) value " + valueName + " has been " + change + " attribute "
					+ attChange.getAttribute().getName() + " of object " + objectName);
		} else if (diff instanceof DiagramDiff) {
			
			DiagramDiff ddiff = (DiagramDiff) diff;
			
			stream.println("[" + count + "] (D) " + ddiff.toString() + " : " + ddiff.getView());
			
		} else {
			stream.println("[" + count + "] (ELSE) " + diff.toString());
		}
	}

	/**
	 * Prints the given {@link Match} on the given {@code stream}.
	 * 
	 * @param match
	 *            The match we are to print on {@code stream}.
	 * @param stream
	 *            The {@link PrintStream} on which we should print this difference.
	 * @see #printMatch(Comparison, PrintStream) A description on how we format the match.
	 */
	private static void printMatch(Match match, PrintStream stream) {
		String leftName = null;
		String rightName = null;
		String originName = null;

		final EObject left = match.getLeft();
		final EObject right = match.getRight();
		final EObject origin = match.getOrigin();

		// Ignore this match if it is not a Named element
		if (isNullOrNamedElement(left) && isNullOrNamedElement(right) && isNullOrNamedElement(origin)) {
			if (left != null && ((ENamedElement)left).getName() != null) {
				leftName = formatName((ENamedElement)left);
			} else {
				int level = 0;
				EObject currentMatch = match;
				while (currentMatch instanceof Match && ((Match)currentMatch).getLeft() == null) {
					currentMatch = currentMatch.eContainer();
				}
				while (currentMatch instanceof Match && ((Match)currentMatch).getLeft() != null) {
					level++;
					currentMatch = currentMatch.eContainer();
				}
				leftName = getEmptyLine(level);
			}

			if (right != null && ((ENamedElement)right).getName() != null) {
				rightName = formatName((ENamedElement)right);
			} else {
				int level = 0;
				EObject currentMatch = match;
				while (currentMatch instanceof Match && ((Match)currentMatch).getRight() == null) {
					currentMatch = currentMatch.eContainer();
				}
				while (currentMatch instanceof Match && ((Match)currentMatch).getRight() != null) {
					level++;
					currentMatch = currentMatch.eContainer();
				}
				rightName = getEmptyLine(level);
			}

			if (origin != null && ((ENamedElement)origin).getName() != null) {
				originName = formatName((ENamedElement)origin);
			} else {
				int level = 0;
				EObject currentMatch = match;
				while (currentMatch instanceof Match && ((Match)currentMatch).getOrigin() == null) {
					currentMatch = currentMatch.eContainer();
				}
				while (currentMatch instanceof Match && ((Match)currentMatch).getOrigin() != null) {
					level++;
					currentMatch = currentMatch.eContainer();
				}
				originName = getEmptyLine(level);
			}

			stream.println('|' + leftName + '|' + rightName + '|' + originName + '|');
		}

		for (Match submatch : match.getSubmatches()) {
			printMatch(submatch, stream);
		}
	}

	/**
	 * Formats the given header so that it spans {@value #COLUMN_LENGTH} characters, centered between white
	 * spaces.
	 * 
	 * @param header
	 *            The header we are to format.
	 * @return The formatted header.
	 */
	private static String formatHeader(String header) {
		int padding = (COLUMN_LENGTH - header.length()) / 2;
		char[] charsBefore = new char[padding];
		for (int i = 0; i < charsBefore.length; i++) {
			charsBefore[i] = ' ';
		}
		if ((header.length() & 1) == 1) {
			padding++;
		}
		final char[] charsAfter = new char[padding];
		for (int i = 0; i < charsAfter.length; i++) {
			charsAfter[i] = ' ';
		}
		return String.valueOf(charsBefore) + header + String.valueOf(charsAfter);
	}

	/**
	 * Formats the named of the given element by adding spaces before and after it so that it spans
	 * {@value #COLUMN_LENGTH} characters at most.
	 * 
	 * @param element
	 *            The element which name should be formatted.
	 * @return the formatted element's name.
	 */
	private static String formatName(ENamedElement element) {
		String name = element.getName();
		int level = 0;
		EObject current = element;
		while (current.eContainer() != null) {
			level++;
			current = current.eContainer();
		}

		char[] charsBefore = new char[1 + (level * 2)];
		charsBefore[0] = ' ';
		if (level > 0) {
			for (int i = 1; i < charsBefore.length - 2; i = i + 2) {
				charsBefore[i] = '|';
				charsBefore[i + 1] = ' ';
			}
			charsBefore[charsBefore.length - 2] = '|';
			charsBefore[charsBefore.length - 1] = '-';
		}

		int missingChars = COLUMN_LENGTH - name.length() - charsBefore.length;
		final char[] spacesAfter = new char[Math.max(0, missingChars)];
		Arrays.fill(spacesAfter, ' ');

		return String.valueOf(charsBefore) + name + String.valueOf(spacesAfter);
	}

	/**
	 * Returns an "empty line" which will only show pipes for previous levels.
	 * 
	 * @param level
	 *            The level of nesting that we should make visible through pipes on this line.
	 * @return A line that displays only pipes for a tree's {@code level}, and only that.
	 */
	private static String getEmptyLine(int level) {
		char[] charsBefore = new char[1 + (level * 2)];
		charsBefore[0] = ' ';
		for (int i = 1; i < charsBefore.length; i = i + 2) {
			charsBefore[i] = '|';
			charsBefore[i + 1] = ' ';
		}

		int missingChars = COLUMN_LENGTH - charsBefore.length;
		final char[] spacesAfter = new char[Math.max(0, missingChars)];
		Arrays.fill(spacesAfter, ' ');

		return String.valueOf(charsBefore) + String.valueOf(spacesAfter);
	}

	/**
	 * Returns <code>true</code> if the given {@code object} is either <code>null</code> or an instance of
	 * {@link ENamedElement}.
	 * 
	 * @param object
	 *            The object we'll test here.
	 * @return <code>true</code> if the given {@code object} is either <code>null</code> or an instance of
	 *         {@link ENamedElement}.
	 */
	private static boolean isNullOrNamedElement(final Object object) {
		return object == null || object instanceof ENamedElement;
	}
}
