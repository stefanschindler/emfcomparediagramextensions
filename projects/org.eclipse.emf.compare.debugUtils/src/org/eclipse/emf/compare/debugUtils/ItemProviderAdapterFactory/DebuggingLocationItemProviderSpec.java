/******************************************************************************
 * Copyright (c) 2004, 2006, 2018 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    IBM Corporation - initial API and implementation
 *    Stefan Schindler - debugging adaptions
 ****************************************************************************/

package org.eclipse.emf.compare.debugUtils.ItemProviderAdapterFactory;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.gmf.runtime.notation.provider.LocationItemProvider;

public class DebuggingLocationItemProviderSpec extends LocationItemProvider {

	public DebuggingLocationItemProviderSpec(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}
	
	@Override
	public String getText(Object obj) {
		
		String ret = "";
		
		ret = super.getText(obj);
		
		ret += ret.length() > 0?" (":"(";
		ret += obj.getClass().getSimpleName();
		ret += "@" + Integer.toHexString(obj.hashCode());
		ret += ")";
		return ret;
	}

}
