/*******************************************************************************
* Copyright (c) 2013, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.debugUtils.filter;

import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.debugUtils.console.EMFCompareDebugPrettyPrinter;
import org.eclipse.emf.compare.rcp.ui.internal.structuremergeviewer.nodes.DiffNode;
import org.eclipse.emf.compare.rcp.ui.structuremergeviewer.filters.AbstractDifferenceFilter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edit.tree.TreeNode;

import com.google.common.base.Predicate;

public class DummyFilter extends AbstractDifferenceFilter {

	/**
	 * The predicate use by this filter when it is selected.
	 */
	private static final Predicate<? super EObject> PREDICATE_WHEN_SELECTED = new Predicate<EObject>() {
		
		private int count = 0;

		@SuppressWarnings("unused")
		public boolean apply(EObject input) {
			
			boolean ret = false;
			
			if(count == 0) {
				System.out.println("myDummyFilter(mdf) - list of input objects:");
			}
			count++;
			
			if (input instanceof TreeNode) {
				
				final EObject data = ((TreeNode) input).getData();
				
				if (data instanceof ReferenceChange) {
					
					final EReference reference = ((ReferenceChange) data).getReference();
					
					Diff diff = (Diff) ((DiffNode) input).getData();
					
					final ReferenceChange refChange = (ReferenceChange)diff;
					
					System.out.print("mdf(" + count + ") ");
					
					EMFCompareDebugPrettyPrinter.printDiff(0, diff, System.out);
					
									
				} else {
					
					System.out.println("mdf(" + count + ")  !RC - " +  input.getClass()  + " : " + input.toString());
				}
				
			} else {
				
				System.out.println("mdf(" + count + ")  !TN - " +  input.getClass()  + " : " + input.toString());
			}
			
			return ret;
			
			/*if(count == 0) {
				System.out.println("myDummyFilter: first call!");
			}
			System.out.println("DummyFilter.calls=" + count++);*/
						
			/*if (input instanceof TreeNode) {
				TreeNode treeNode = (TreeNode)input;
				if (treeNode.getData() instanceof MatchResource) {
					ret = treeNode.getChildren().isEmpty();
				}
			}*/
			
			
			/*if(count == 0) {
				System.out.println("myDummyFilter - list of input objects:");
			}
			count++;
			System.out.println("(" + count + ")" +  input.getClass()  + " : " + input.toString() );*/
			
			//get LabelProvider
			//check Labels
		}
	};

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.rcp.ui.structuremergeviewer.filters.IDifferenceFilter#getPredicateWhenSelected()
	 */
	@Override
	public Predicate<? super EObject> getPredicateWhenSelected() {
		return PREDICATE_WHEN_SELECTED;
	}

}
