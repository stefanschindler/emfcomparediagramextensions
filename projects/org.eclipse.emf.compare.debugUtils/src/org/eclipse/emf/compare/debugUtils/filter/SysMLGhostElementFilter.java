/*******************************************************************************
* Copyright (c) 2013, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.debugUtils.filter;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.rcp.ui.structuremergeviewer.filters.AbstractDifferenceFilter;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.tree.TreeNode;
import org.eclipse.gmf.runtime.notation.Connector;
import org.eclipse.gmf.runtime.notation.Shape;

import com.google.common.base.Predicate;

public class SysMLGhostElementFilter extends AbstractDifferenceFilter {
	
	private static final String TYPE_STEREOTYPE_COMMENT = "StereotypeComment";
	private static final String TYPE_STEREOTYPE_COMMENT_LINK = "StereotypeCommentLink";

	private static final Predicate<? super EObject> PREDICATE_WHEN_SELECTED = new Predicate<EObject>() {
		
		//@Override
		public boolean apply(EObject input) {
			
			boolean ret = false; //return value, true => input object gets filtered from output
			
			if (input instanceof TreeNode) {
				
				EObject data = ((TreeNode) input).getData();
				
				if (data instanceof ReferenceChange) {
					
					//check if sysml diagram?
					//check if connection to block element
					
					ReferenceChange refChange = (ReferenceChange) data;
					
					//check if refining high
					
					if(refChange.getValue() instanceof Shape) {
						
						Shape shape = (Shape) refChange.getValue();
						
						//check visibility of shape!!
						ret = shape.getType() == TYPE_STEREOTYPE_COMMENT;
						
						if(ret) {
							
							boolean visible = shape.isVisible();
														
							
							System.out.println("SysMLGhostElementFilter - " + TYPE_STEREOTYPE_COMMENT + "(@" + Integer.toHexString(shape.hashCode()) + ") : visible=" + visible);
						}
						
					} else if (refChange.getValue() instanceof Connector) {
						
						Connector con = (Connector) refChange.getValue();
						
						//check visibility of target shape!!
						ret = con.getType() == TYPE_STEREOTYPE_COMMENT_LINK;
						
						if(ret) {
							
							boolean visible = con.isVisible();
							
							Shape source = (Shape) con.getSource();
							Shape target = (Shape) con.getTarget();
							
							System.out.println("SysMLGhostElementFilter - " + TYPE_STEREOTYPE_COMMENT_LINK + "(@" + Integer.toHexString(con.hashCode()) + ") : visible=" + visible);
							System.out.println("  source: " + source.getClass().getSimpleName() + "(@" + Integer.toHexString(source.hashCode()) + "), "+ source.getType() +" : visible=" + source.isVisible());
							System.out.println("  target: " + target.getClass().getSimpleName() + "(@" + Integer.toHexString(target.hashCode()) + "), "+ target.getType() +" : visible=" + target.isVisible());
						}
					}			
				}
				
				/*
				if(ret){
					
					Diff diff = (Diff) ((DiffNode) input).getData();
					
					System.out.println("ghost (" + data.getClass()  + " : " + data.toString() + ")");
					
					System.out.print("& ");
					
					EMFComparePrettyPrinter.printDiff(diff, System.out);
				}
				*/
			}
			
			return ret;
		}
	};

	@Override
	public Predicate<? super EObject> getPredicateWhenSelected() {
		return PREDICATE_WHEN_SELECTED;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.rcp.ui.structuremergeviewer.filters.IDifferenceFilter#isEnabled(org.eclipse.emf.compare.scope.IComparisonScope,
	 *      org.eclipse.emf.compare.Comparison)
	 */
	@Override
	public boolean isEnabled(IComparisonScope scope, Comparison comparison) {
		
		if (scope != null) {
		
			boolean ret = false;
			String targetNsURI = "http://www\\.eclipse\\.org/papyrus/.*/SysML/.*";
			
			for (String nsURI : scope.getNsURIs()) {
				
				if (nsURI.matches(targetNsURI)) {
					
					ret = true;
					break;
				}
			}
			
			return ret;
		}
				
		return false;
	}

}
