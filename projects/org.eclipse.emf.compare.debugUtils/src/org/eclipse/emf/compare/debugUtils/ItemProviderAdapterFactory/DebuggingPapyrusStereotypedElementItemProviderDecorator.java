/*******************************************************************************
 * Copyright (c) 2014, 2016, 2018 Obeo and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Obeo - initial API and implementation
 *     Alexandra Buzila - bug 483798
 *     Stefan Schindler - debugging adaptions
 *******************************************************************************/

package org.eclipse.emf.compare.debugUtils.ItemProviderAdapterFactory;

import org.eclipse.emf.compre.uml2.edit.papyrus.internal.decorator.PapyrusStereotypedElementItemProviderDecorator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;

public class DebuggingPapyrusStereotypedElementItemProviderDecorator extends PapyrusStereotypedElementItemProviderDecorator {

	public DebuggingPapyrusStereotypedElementItemProviderDecorator(ComposeableAdapterFactory adapterFactory) {

		super(adapterFactory);
	}
	
	@Override
	public String getText(Object obj) {
		
		String ret = "";
		
		ret = super.getText(obj);
		
		ret += ret.length() > 0?" (":"(";
		ret += obj.getClass().getSimpleName();
		ret += "@" + Integer.toHexString(obj.hashCode());
		ret += ")";
		return ret;
	}

}
