/*******************************************************************************
* Copyright (c) 2013, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - debugging adaptions
*******************************************************************************/

package org.eclipse.emf.compare.debugUtils.ItemProviderAdapterFactory;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.compare.papyrus.internal.matchs.provider.spec.PapyrusCompareItemProviderAdapterFactorySpec;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Location;
import org.eclipse.gmf.runtime.notation.Style;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.util.NotationSwitch;

public class DebuggingPapyrusCompareItemProviderAdapterFactorySpec extends PapyrusCompareItemProviderAdapterFactorySpec {

	
	public DebuggingPapyrusCompareItemProviderAdapterFactorySpec() {
		super();
	}

	protected NotationSwitch debuggingModelSwitch = new NotationSwitch() {
		
		@Override
		public Object caseView(View object) {
			
			return new DebuggingViewItemProviderSpec(DebuggingPapyrusCompareItemProviderAdapterFactorySpec.this);
		}
		
		@Override
		public Object caseDiagram(Diagram object) {
			
			return new DebuggingDiagramItemProviderSpec(DebuggingPapyrusCompareItemProviderAdapterFactorySpec.this);
		}

		@Override
		public Object defaultCase(EObject object) {
			
			/*
			if (object instanceof Style) {
				
				return new DebuggingStyleItemProviderSpec(DebuggingPapyrusCompareItemProviderAdapterFactorySpec.this);
				
			} else if (object instanceof Location){
				
				return new DebuggingLocationItemProviderSpec(DebuggingPapyrusCompareItemProviderAdapterFactorySpec.this);
				
			}
			*/
			
			/*
			else {
				
				return new DebuggingItemProviderSpec(DebuggingPapyrusCompareItemProviderAdapterFactorySpec.this);
			}
			*/

			// delegate to the default notational switch
			return modelSwitch.doSwitch(object);
		}

	};
	
	@Override
	public Adapter createAdapter(Notifier target) {
		return (Adapter)debuggingModelSwitch.doSwitch((EObject)target);
	}
}
