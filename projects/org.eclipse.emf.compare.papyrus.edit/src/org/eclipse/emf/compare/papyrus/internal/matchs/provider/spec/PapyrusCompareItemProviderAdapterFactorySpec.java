/*******************************************************************************
* Copyright (c) 2013, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal.matchs.provider.spec;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.compare.diagram.internal.matchs.provider.spec.DiagramCompareItemProviderAdapterFactorySpec;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.util.NotationSwitch;

public class PapyrusCompareItemProviderAdapterFactorySpec extends DiagramCompareItemProviderAdapterFactorySpec {

	public PapyrusCompareItemProviderAdapterFactorySpec() {
		super();
	}

	protected NotationSwitch papyrusModelSwitchSpec = new NotationSwitch() {
		
		@Override
		public Object caseView(View object) {
			return new PapyrusViewItemProviderSpec(PapyrusCompareItemProviderAdapterFactorySpec.this);
		}
		
		@Override
		public Object defaultCase(EObject object) {
			
			return modelSwitchSpec.doSwitch(object);
		}

	};
	
	@Override
	public Adapter createAdapter(Notifier target) {
		return (Adapter)papyrusModelSwitchSpec.doSwitch((EObject)target);
	}
}