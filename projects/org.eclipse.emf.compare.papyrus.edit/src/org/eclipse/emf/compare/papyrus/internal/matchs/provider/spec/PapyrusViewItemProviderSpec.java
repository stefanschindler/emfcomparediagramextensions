/*******************************************************************************
* Copyright (c) 2012, 2013, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal.matchs.provider.spec;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.compare.diagram.internal.matchs.provider.spec.ViewItemProviderSpec;
import org.eclipse.emf.compare.provider.AdapterFactoryUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.View;

public class PapyrusViewItemProviderSpec extends ViewItemProviderSpec {

	public PapyrusViewItemProviderSpec(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}
	
	@Override
	protected String getElementText(View obj) {
		EObject element = obj.getElement();
		String ret = "";
		if (element != null) {
			ret = AdapterFactoryUtil.getText(getRootAdapterFactory(), element);
		} else {
			ret = "<" + obj.getType() + ">";
		}
		return ret;
	}
	
//	@Override
//	public Object getImage(Object object) {
//		EObject obj = (View)object;
//		return overlayImage(object,
//				PapyrusCompareEditPlugin.INSTANCE.getImage("full/obj16/" + obj.eClass().getName()));
//	}

}
