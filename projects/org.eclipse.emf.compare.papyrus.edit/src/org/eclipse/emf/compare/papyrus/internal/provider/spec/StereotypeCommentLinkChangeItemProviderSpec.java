/*******************************************************************************
* Copyright (c) 2013, 2015, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal.provider.spec;

import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.diagram.internal.extensions.DiagramDiff;
import org.eclipse.emf.compare.diagram.internal.extensions.provider.DiagramDiffItemProvider;
import org.eclipse.emf.compare.diagram.internal.extensions.provider.spec.ForwardingDiagramDiffItemProvider;
import org.eclipse.emf.compare.provider.spec.OverlayImageProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.gmf.runtime.notation.Connector;

public class StereotypeCommentLinkChangeItemProviderSpec extends ForwardingDiagramDiffItemProvider {

	private final OverlayImageProvider fOverlayProvider;
	
	public StereotypeCommentLinkChangeItemProviderSpec(ItemProviderAdapter delegate) {
		super(delegate);
		fOverlayProvider = new OverlayImageProvider(
				((DiagramDiffItemProvider)delegate).getResourceLocator());
	}
	
	@Override
	protected String getReferenceText(final DiagramDiff diagramDiff) {
		
		String ret = "";
		Diff diff = diagramDiff.getPrimeRefining();
		
		if(diff instanceof ReferenceChange) {
			
			ret = ((ReferenceChange) diff).getReference().getName();
			
		}
		return ret;
	}
	
	@Override
	protected String getValueText(final DiagramDiff diagramDiff) {
		String ret = getValueText(diagramDiff.getView());
		
		if(!isVisible(diagramDiff)) {
			ret = "Invisible " + ret;
		}
		
		return ret;
	}
	
	@Override
	public Object getImage(Object object) {
		
		final DiagramDiff diagramDiff = (DiagramDiff)object;
		String path = "full/obj16/StereotypeCommentLinkChange";
		
		if(!isVisible(diagramDiff)) {
			path += "Invisible";
		}
		
		Object image = ((DiagramDiffItemProvider) delegate()).getOverlayImage(object, ((DiagramDiffItemProvider) delegate()).getResourceLocator().getImage(path)); //$NON-NLS-1$
		
		if (fOverlayProvider != null && image != null) {
			Object diffImage = fOverlayProvider.getComposedImage(diagramDiff, image);
			return ((DiagramDiffItemProvider)delegate()).getOverlayImage(object, diffImage);
		} else {
			return image;
		}
	}
	
	private boolean isVisible(DiagramDiff dd) {
		boolean ret = false;
		
		Connector con = (Connector) dd.getView();
		ret = con.getTarget().isVisible();
		
		return ret;
	}
	
}
