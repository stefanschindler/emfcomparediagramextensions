/*******************************************************************************
* Copyright (c) 2013, 2015, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal.provider.spec;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.compare.papyrus.internal.provider.PapyrusItemProviderAdapterFactory;
import org.eclipse.emf.compare.provider.IItemDescriptionProvider;
import org.eclipse.emf.compare.provider.IItemStyledLabelProvider;
import org.eclipse.emf.compare.provider.ISemanticObjectLabelProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

public class PapyrusItemProviderAdapterFactorySpec extends PapyrusItemProviderAdapterFactory {

	protected StereotypeCommentChangeItemProviderSpec stereotypeCommentChangeItemProvider;
	protected StereotypeCommentLinkChangeItemProviderSpec stereotypeCommentLinkChangeItemProvider;
	
	
	public PapyrusItemProviderAdapterFactorySpec() {
		super();
		supportedTypes.add(IItemStyledLabelProvider.class);
		supportedTypes.add(IItemDescriptionProvider.class);
		supportedTypes.add(ISemanticObjectLabelProvider.class);
	}

	@Override
	public Adapter createStereotypeCommentChangeAdapter() {
		if (stereotypeCommentChangeItemProvider == null) {
			stereotypeCommentChangeItemProvider = new StereotypeCommentChangeItemProviderSpec(
					(ItemProviderAdapter) super.createStereotypeCommentChangeAdapter());
		}

		return stereotypeCommentChangeItemProvider;
	}
	

	@Override
	public Adapter createStereotypeCommentLinkChangeAdapter() {
		if (stereotypeCommentLinkChangeItemProvider == null) {
			stereotypeCommentLinkChangeItemProvider = new StereotypeCommentLinkChangeItemProviderSpec(
					(ItemProviderAdapter) super.createStereotypeCommentLinkChangeAdapter());
		}

		return stereotypeCommentLinkChangeItemProvider;
	}
}
