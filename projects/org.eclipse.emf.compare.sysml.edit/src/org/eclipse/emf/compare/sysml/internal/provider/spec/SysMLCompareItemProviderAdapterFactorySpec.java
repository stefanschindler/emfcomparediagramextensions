/*******************************************************************************
* Copyright (c) 2013, 2015, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.sysml.internal.provider.spec;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.compare.provider.IItemDescriptionProvider;
import org.eclipse.emf.compare.provider.IItemStyledLabelProvider;
import org.eclipse.emf.compare.provider.ISemanticObjectLabelProvider;
import org.eclipse.emf.compare.sysml.internal.provider.SysMLCompareItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;


public class SysMLCompareItemProviderAdapterFactorySpec extends SysMLCompareItemProviderAdapterFactory {
	
	protected PortChangeItemProviderSpec portChangeItemProvider;
	
	public SysMLCompareItemProviderAdapterFactorySpec() {
		super();
		supportedTypes.add(IItemStyledLabelProvider.class);
		supportedTypes.add(IItemDescriptionProvider.class);
		supportedTypes.add(ISemanticObjectLabelProvider.class);
	}

	@Override
	public Adapter createPortChangeAdapter() {
		if (portChangeItemProvider == null) {
			portChangeItemProvider = new PortChangeItemProviderSpec((ItemProviderAdapter) super.createPortChangeAdapter());
		}

		return portChangeItemProvider;
	}
}
