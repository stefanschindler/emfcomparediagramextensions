/*******************************************************************************
* Copyright (c) 2012, 2016, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.rcp.ui.internal.structuremergeviewer.groups.impl;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.diagram.internal.extensions.DiagramDiff;
import org.eclipse.emf.compare.rcp.ui.internal.structuremergeviewer.groups.impl.BasicDifferenceGroupImpl;
import org.eclipse.emf.compare.rcp.ui.internal.structuremergeviewer.nodes.DiffNode;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.swt.graphics.Image;

import com.google.common.base.Predicate;

public class PapyrusBasicDifferenceGroupImpl extends BasicDifferenceGroupImpl {

	public PapyrusBasicDifferenceGroupImpl(Comparison comparison, Predicate<? super Diff> filter,
			ECrossReferenceAdapter crossReferenceAdapter) {
		super(comparison, filter, crossReferenceAdapter);
	}

	public PapyrusBasicDifferenceGroupImpl(Comparison comparison, Predicate<? super Diff> filter, String name,
			ECrossReferenceAdapter crossReferenceAdapter) {
		super(comparison, filter, name, crossReferenceAdapter);
	}
	
	public PapyrusBasicDifferenceGroupImpl(Comparison comparison, Predicate<? super Diff> filter, String name,
			Image image, ECrossReferenceAdapter crossReferenceAdapter) {
		super(comparison, filter, name, image, crossReferenceAdapter);
	}
	
	@Override
	protected void handleRefiningDiffs(DiffNode diffNode) {
		Diff diff = diffNode.getDiff();
		for (Diff refiningDiff : diff.getRefinedBy()) {
			
			if (diff instanceof DiagramDiff && refiningDiff instanceof ReferenceChange && 
					 ((DiagramDiff)diff).getView().equals(((ReferenceChange)refiningDiff).getValue())) {
				
				continue;
			}
			if (diff instanceof DiagramDiff && refiningDiff instanceof DiagramDiff && 
					 diff.getPrimeRefining().equals(refiningDiff.getPrimeRefining())) {
				
				continue;
			}
			DiffNode refinedDiffNode = createDiffNode(refiningDiff);
			diffNode.addRefinedDiffNode(refinedDiffNode);
			handleRefiningDiffs(refinedDiffNode);
		}
	}

}
