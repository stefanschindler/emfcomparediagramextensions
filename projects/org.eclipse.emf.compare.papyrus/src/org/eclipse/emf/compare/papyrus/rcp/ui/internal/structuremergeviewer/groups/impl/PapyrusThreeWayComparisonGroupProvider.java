/*******************************************************************************
* Copyright (c) 2013, 2016, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Dirix - bug 488941
*     Simon Delisle, Edgar Mueller - bug 486923
*     Tanja Mayerhofer - bug 501864
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.rcp.ui.internal.structuremergeviewer.groups.impl;

import static com.google.common.base.Predicates.and;
import static org.eclipse.emf.compare.utils.EMFComparePredicates.fromSide;

import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.DifferenceSource;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.diagram.internal.extensions.DiagramDiff;
import org.eclipse.emf.compare.rcp.ui.internal.EMFCompareRCPUIMessages;
import org.eclipse.emf.compare.rcp.ui.internal.configuration.SideLabelProvider;
import org.eclipse.emf.compare.rcp.ui.internal.structuremergeviewer.groups.impl.BasicDifferenceGroupImpl;
import org.eclipse.emf.compare.rcp.ui.internal.structuremergeviewer.groups.impl.ThreeWayComparisonGroupProvider;
import org.eclipse.emf.compare.rcp.ui.internal.structuremergeviewer.nodes.DiffNode;
import org.eclipse.emf.compare.rcp.ui.structuremergeviewer.groups.IDifferenceGroup;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

public class PapyrusThreeWayComparisonGroupProvider extends ThreeWayComparisonGroupProvider {

	public static class PapyrusConflictsGroupImpl extends ThreeWayComparisonGroupProvider.ConflictsGroupImpl {

		public PapyrusConflictsGroupImpl(Comparison comparison, Predicate<? super Diff> filter, String name,
				ECrossReferenceAdapter crossReferenceAdapter) {
			super(comparison, filter, name, crossReferenceAdapter);
			
		}
		
		public PapyrusConflictsGroupImpl(Comparison comparison, String name,
				ECrossReferenceAdapter crossReferenceAdapter) {
			super(comparison, name, crossReferenceAdapter);
		}
		
		@Override
		protected void handleRefiningDiffs(DiffNode diffNode) {
			Diff diff = diffNode.getDiff();
			for (Diff refiningDiff : diff.getRefinedBy()) {
				
				if (diff instanceof DiagramDiff && refiningDiff instanceof ReferenceChange && 
						 ((DiagramDiff)diff).getView() == ((ReferenceChange)refiningDiff).getValue()) {
					
					continue;
				}
				if (diff instanceof DiagramDiff && refiningDiff instanceof DiagramDiff && 
						 diff.getPrimeRefining().equals(refiningDiff.getPrimeRefining())) {
					
					continue;
				}
				DiffNode refinedDiffNode = createDiffNode(refiningDiff);
				diffNode.addRefinedDiffNode(refinedDiffNode);
				handleRefiningDiffs(refinedDiffNode);
			}
		}
		
	}
	
	@Override
	protected Collection<? extends IDifferenceGroup> buildGroups(Comparison comparison2) {
		Adapter adapter = EcoreUtil.getAdapter(getComparison().eAdapters(), SideLabelProvider.class);

		final String leftLabel;
		final String rightLabel;

		if (adapter instanceof SideLabelProvider) {
			SideLabelProvider labelProvider = (SideLabelProvider)adapter;
			leftLabel = labelProvider.getLeftLabel();
			rightLabel = labelProvider.getRightLabel();
		} else {
			leftLabel = EMFCompareRCPUIMessages.getString("ThreeWayComparisonGroupProvider.left.label"); //$NON-NLS-1$
			rightLabel = EMFCompareRCPUIMessages.getString("ThreeWayComparisonGroupProvider.right.label"); //$NON-NLS-1$
		}

		final ConflictsGroupImpl conflicts = new PapyrusConflictsGroupImpl(getComparison(),
				EMFCompareRCPUIMessages.getString("ThreeWayComparisonGroupProvider.conflicts.label"), //$NON-NLS-1$
				getCrossReferenceAdapter());
		conflicts.buildSubTree();

		final BasicDifferenceGroupImpl leftSide = new PapyrusBasicDifferenceGroupImpl(getComparison(),
				and(fromSide(DifferenceSource.LEFT), DEFAULT_DIFF_GROUP_FILTER_PREDICATE), leftLabel,
				getCrossReferenceAdapter());
		leftSide.buildSubTree();

		final BasicDifferenceGroupImpl rightSide = new PapyrusBasicDifferenceGroupImpl(getComparison(),
				and(fromSide(DifferenceSource.RIGHT), DEFAULT_DIFF_GROUP_FILTER_PREDICATE), rightLabel,
				getCrossReferenceAdapter());
		rightSide.buildSubTree();

		return ImmutableList.of(conflicts, leftSide, rightSide);
	}
	
}
