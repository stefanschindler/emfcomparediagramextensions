/*******************************************************************************
* Copyright (c) 2013, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.diagram.ide.ui.internal.structuremergeviewer.filter;

import org.eclipse.emf.compare.papyrus.internal.StereotypeCommentChange;
import org.eclipse.emf.compare.papyrus.internal.StereotypeCommentLinkChange;
import org.eclipse.emf.compare.rcp.ui.structuremergeviewer.filters.AbstractDifferenceFilter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.tree.TreeNode;
import org.eclipse.gmf.runtime.notation.Connector;
import org.eclipse.gmf.runtime.notation.Shape;

import com.google.common.base.Predicate;

public class StereotypeCommentFilter extends AbstractDifferenceFilter {
	
	private static final String TYPE_STEREOTYPE_COMMENT = "StereotypeComment";
	private static final String TYPE_STEREOTYPE_COMMENT_LINK = "StereotypeCommentLink";
	//private static final String TARGET_NSURI = "http://www\\.eclipse\\.org/papyrus/.*/SysML/.*";

	private static final Predicate<? super EObject> PREDICATE_WHEN_SELECTED = new Predicate<EObject>() {
		
		//@Override
		public boolean apply(EObject input) {
			
			boolean ret = false; //return value, true => input object gets filtered from output
			
			if (input instanceof TreeNode) {
				
				EObject data = ((TreeNode) input).getData();
				
				if (data instanceof StereotypeCommentChange) {
					
					StereotypeCommentChange scc = (StereotypeCommentChange) data;
					Shape shape = (Shape) scc.getView();
						
					if(shape.getType() == TYPE_STEREOTYPE_COMMENT) {
							
						ret = !shape.isVisible();	
					}
						
				} else if (data instanceof StereotypeCommentLinkChange) {
						
					StereotypeCommentLinkChange sclc = (StereotypeCommentLinkChange) data;
					Connector con = (Connector) sclc.getView();
						
					if(con.getType() == TYPE_STEREOTYPE_COMMENT_LINK) {
							
						Shape target = (Shape) con.getTarget();
						ret = !target.isVisible();
					}
				}			
			}
		
			return ret;
		}
	};

	@Override
	public Predicate<? super EObject> getPredicateWhenSelected() {
		return PREDICATE_WHEN_SELECTED;
	}
	
	/*
	@Override
	public boolean isEnabled(IComparisonScope scope, Comparison comparison) {
		
		boolean ret = false;
		
		if (scope != null) {
		
			String targetNsURI = TARGET_NSURI;
			
			for (String nsURI : scope.getNsURIs()) {
				
				if (nsURI.matches(targetNsURI)) {
					
					ret = true;
					break;
				}
			}
		}
				
		return ret;
	}
	*/

}

