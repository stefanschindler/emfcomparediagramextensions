/*******************************************************************************
* Copyright (c) 2013, 2015, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal;

import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.DifferenceKind;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.ResourceAttachmentChange;
import org.eclipse.emf.compare.diagram.internal.extensions.DiagramDiff;
import org.eclipse.emf.compare.diagram.internal.factories.extensions.NodeChangeFactory;
import org.eclipse.emf.compare.internal.utils.ComparisonUtil;
import org.eclipse.emf.compare.util.CompareSwitch;
import org.eclipse.emf.compare.utils.EMFComparePredicates;
import org.eclipse.emf.compare.utils.MatchUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Shape;

import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;

public class StereotypeCommentChangeFactory extends NodeChangeFactory {

	private static final String TYPE_STEREOTYPE_COMMENT = "StereotypeComment";
	
	
	public StereotypeCommentChangeFactory() {
		
	}

	@Override
	public Class<? extends Diff> getExtensionKind() {
		return StereotypeCommentChange.class;
	}
	
	@Override
	public DiagramDiff createExtension() {
		
		return PapyrusFactory.eINSTANCE.createStereotypeCommentChange();
	}
	
	@Override
	protected boolean isRelatedToAnExtensionAdd(ReferenceChange input) {
		
		boolean ret = isRelatedToAnExtensionKind(input, DifferenceKind.ADD);		
				
		return ret;
	}
	
	@Override
	protected boolean isRelatedToAnExtensionDelete(ReferenceChange input) {
		
		boolean ret = isRelatedToAnExtensionKind(input, DifferenceKind.DELETE);		
		
		return ret;
	}
	
	@Override
	protected boolean isRelatedToAnExtensionMove(ReferenceChange input) {
		
		boolean ret = isRelatedToAnExtensionKind(input, DifferenceKind.MOVE);		
		
		return ret;
	}
	
	protected boolean isRelatedToAnExtensionKind(ReferenceChange input, DifferenceKind kind) {
		
		boolean ret = false;
		
		if(input.getKind() == kind) {
			
			ret = isStereotypeCommentShape(input);
			
		}
		
		return ret;
		
	}

	protected boolean isStereotypeCommentShape(ReferenceChange input) {
		
		boolean ret = false;
		
		EObject value = input.getValue();
		
		if(value instanceof Shape) {
			
			ret = ((Shape) value).getType() == TYPE_STEREOTYPE_COMMENT;
			
		}
		
		return ret;
	}
	
	@Override
	protected Set<Diff> getAllContainedDifferences(Diff input) {
		final Set<Diff> result = new LinkedHashSet<Diff>();

		final Comparison comparison = ComparisonUtil.getComparison(input);

		CompareSwitch<EObject> valueGetter = new CompareSwitch<EObject>() {
			@Override
			public EObject caseReferenceChange(ReferenceChange object) {
				return object.getValue();
			}

			@Override
			public EObject caseResourceAttachmentChange(ResourceAttachmentChange object) {
				return MatchUtil.getContainer(ComparisonUtil.getComparison(object), object);
			}

			@Override
			public EObject defaultCase(EObject object) {
				return null;
			}
		};
		EObject value = valueGetter.doSwitch(input);

		if (value != null) {
			final Match match = comparison.getMatch(value);
			result.addAll(getAllContainedDifferences(comparison, match));
		}

		return result;
	}
	
	private Set<Diff> getAllContainedDifferences(Comparison comparison, Match match) {
		final Set<Diff> result = Sets.newLinkedHashSet();

		final Set<Match> prune = Sets.newLinkedHashSet();
		for (Diff candidate : match.getDifferences()) {
			// Keep only unit changes...
			if (!getExtensionKind().isInstance(candidate) && candidate.getRefines().isEmpty()) {
				// ... which are not related to an other macroscopic ADD or DELETE or MOVE of a graphical
				// object.
				if (getRelatedExtensionKind(candidate) == null) {
					result.add(candidate);
				} else if (candidate instanceof ReferenceChange
						&& ((ReferenceChange)candidate).getReference().isContainment()) {
					// match of an added or deleted graphical object.
					prune.add(comparison.getMatch(((ReferenceChange)candidate).getValue()));
				}
			}
		}

		// Re-iterate the research in sub matches of expected objects.
		for (Match submatch : match.getSubmatches()) {
			if (!prune.contains(submatch)) {
				result.addAll(getAllContainedDifferences(comparison, submatch));
			}
		}

		return result;
	}
	
	@Override
	public void fillRequiredDifferences(Comparison comparison, Diff extension) {
		fillRequiredDifferencesForMacroToMacro(extension);
		fillRequiredDifferencesForUnitToMacro(extension);
	}
	
	private void fillRequiredDifferencesForMacroToMacro(Diff macro) {
		
		Set<Diff> requiredExtensions = new LinkedHashSet<Diff>();
		Set<Diff> requiringExtensions = new LinkedHashSet<Diff>();
		for (Diff refiningDiff : macro.getRefinedBy()) {
			
			for (Diff unit : refiningDiff.getRequires()) {
				
				requiredExtensions.addAll(unit.getRefines());
			}
			
			for (Diff unit : refiningDiff.getRequiredBy()) {
				
				requiringExtensions.addAll(unit.getRequiredBy());
			}
		}
		
		requiredExtensions.remove(macro);
		requiringExtensions.remove(macro);

		macro.getRequires().addAll(
				Collections2.filter(requiredExtensions, EMFComparePredicates.fromSide(macro.getSource())));
		macro.getRequiredBy().addAll(
				Collections2.filter(requiringExtensions, EMFComparePredicates.fromSide(macro.getSource())));
	}
	
	private void fillRequiredDifferencesForUnitToMacro(Diff macro) {
		Set<Diff> requiredExtensions = new LinkedHashSet<Diff>();
		Set<Diff> requiringExtensions = new LinkedHashSet<Diff>();
		for (Diff refiningDiff : macro.getRefinedBy()) {
			for (Diff unit : refiningDiff.getRequires()) {
				if (unit.getRefines().isEmpty()) {
					requiredExtensions.add(unit);
				}
			}
			for (Diff unit : refiningDiff.getRequiredBy()) {
				if (unit.getRefines().isEmpty()) {
					requiringExtensions.add(unit);
				}
			}
		}
		
		requiredExtensions.remove(macro);
		requiringExtensions.remove(macro);
		
		macro.getRequires().addAll(
				Collections2.filter(requiredExtensions, EMFComparePredicates.fromSide(macro.getSource())));
		macro.getRequiredBy().addAll(
				Collections2.filter(requiringExtensions, EMFComparePredicates.fromSide(macro.getSource())));
	}	

}
