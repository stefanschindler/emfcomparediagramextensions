/*******************************************************************************
* Copyright (c) 2013, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.Monitor;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.internal.postprocessor.factories.IChangeFactory;
import org.eclipse.emf.compare.internal.utils.ComparisonUtil;
import org.eclipse.emf.compare.postprocessor.IPostProcessor;

public class PapyrusPostProcessor implements IPostProcessor {
	
	private Map<Class<? extends Diff>, IChangeFactory> factoryMap;
	private Map<Diff, IChangeFactory> extensionsMap;
	
	@Override
	public void postMatch(Comparison comparison, Monitor monitor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postDiff(Comparison comparison, Monitor monitor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postRequirements(Comparison comparison, Monitor monitor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postEquivalences(Comparison comparison, Monitor monitor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postConflicts(Comparison comparison, Monitor monitor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postComparison(Comparison comparison, Monitor monitor) {
		
		factoryMap = PapyrusExtensionFactoryRegistry.createExtensionFactories();
		extensionsMap = new HashMap<Diff, IChangeFactory>();
				
		for(Diff diff : comparison.getDifferences()) {
			for(IChangeFactory factory : factoryMap.values()) {
				if(factory.handles(diff)) {
					createExtension(factory, diff);
				}
			}
		}
		//TODO check for existing extensions
		createRequirements();
	}
	
	private void createExtension(IChangeFactory factory, Diff diff) {
		
		Diff extension = factory.create(diff);
		extension.getPrimeRefining();
		extensionsMap.put(extension, factory);
		
		Match parentMatch = factory.getParentMatch(diff);
		parentMatch.getDifferences().add(extension);
		
	}
	
	private void createRequirements() {
		
		for(Diff extension : extensionsMap.keySet()) {
			IChangeFactory factory = extensionsMap.get(extension);
			factory.fillRequiredDifferences(ComparisonUtil.getComparison(extension), extension);
		}
	}

}
