/*******************************************************************************
* Copyright (c) 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal.impl;

import org.eclipse.emf.compare.diagram.internal.extensions.impl.NodeChangeImpl;

import org.eclipse.emf.compare.papyrus.internal.PapyrusPackage;
import org.eclipse.emf.compare.papyrus.internal.StereotypeCommentChange;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stereotype Comment Change</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StereotypeCommentChangeImpl extends NodeChangeImpl implements StereotypeCommentChange {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StereotypeCommentChangeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PapyrusPackage.Literals.STEREOTYPE_COMMENT_CHANGE;
	}

} //StereotypeCommentChangeImpl
