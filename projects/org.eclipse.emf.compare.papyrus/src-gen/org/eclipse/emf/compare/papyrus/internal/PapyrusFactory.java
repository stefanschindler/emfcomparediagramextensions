/*******************************************************************************
* Copyright (c) 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.emf.compare.papyrus.internal.PapyrusPackage
 * @generated
 */
public interface PapyrusFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PapyrusFactory eINSTANCE = org.eclipse.emf.compare.papyrus.internal.impl.PapyrusFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Stereotype Comment Change</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stereotype Comment Change</em>'.
	 * @generated
	 */
	StereotypeCommentChange createStereotypeCommentChange();

	/**
	 * Returns a new object of class '<em>Stereotype Comment Link Change</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stereotype Comment Link Change</em>'.
	 * @generated
	 */
	StereotypeCommentLinkChange createStereotypeCommentLinkChange();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	PapyrusPackage getPapyrusPackage();

} //PapyrusFactory
