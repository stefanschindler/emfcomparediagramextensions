/*******************************************************************************
* Copyright (c) 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal.impl;

import org.eclipse.emf.compare.papyrus.internal.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PapyrusFactoryImpl extends EFactoryImpl implements PapyrusFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PapyrusFactory init() {
		try {
			PapyrusFactory thePapyrusFactory = (PapyrusFactory)EPackage.Registry.INSTANCE.getEFactory(PapyrusPackage.eNS_URI);
			if (thePapyrusFactory != null) {
				return thePapyrusFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new PapyrusFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PapyrusFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case PapyrusPackage.STEREOTYPE_COMMENT_CHANGE: return createStereotypeCommentChange();
			case PapyrusPackage.STEREOTYPE_COMMENT_LINK_CHANGE: return createStereotypeCommentLinkChange();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StereotypeCommentChange createStereotypeCommentChange() {
		StereotypeCommentChangeImpl stereotypeCommentChange = new StereotypeCommentChangeImpl();
		return stereotypeCommentChange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StereotypeCommentLinkChange createStereotypeCommentLinkChange() {
		StereotypeCommentLinkChangeImpl stereotypeCommentLinkChange = new StereotypeCommentLinkChangeImpl();
		return stereotypeCommentLinkChange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PapyrusPackage getPapyrusPackage() {
		return (PapyrusPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static PapyrusPackage getPackage() {
		return PapyrusPackage.eINSTANCE;
	}

} //PapyrusFactoryImpl
