/*******************************************************************************
* Copyright (c) 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal;

import org.eclipse.emf.compare.diagram.internal.extensions.NodeChange;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stereotype Comment Change</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.emf.compare.papyrus.internal.PapyrusPackage#getStereotypeCommentChange()
 * @model
 * @generated
 */
public interface StereotypeCommentChange extends NodeChange {
} // StereotypeCommentChange
