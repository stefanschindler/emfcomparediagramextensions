/*******************************************************************************
* Copyright (c) 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.papyrus.internal;

import org.eclipse.emf.compare.diagram.internal.extensions.ExtensionsPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.emf.compare.papyrus.internal.PapyrusFactory
 * @model kind="package"
 * @generated
 */
public interface PapyrusPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "papyrus"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/emf/compare/papyrus/1.0"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "papyruscompare"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PapyrusPackage eINSTANCE = org.eclipse.emf.compare.papyrus.internal.impl.PapyrusPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.emf.compare.papyrus.internal.impl.StereotypeCommentChangeImpl <em>Stereotype Comment Change</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.compare.papyrus.internal.impl.StereotypeCommentChangeImpl
	 * @see org.eclipse.emf.compare.papyrus.internal.impl.PapyrusPackageImpl#getStereotypeCommentChange()
	 * @generated
	 */
	int STEREOTYPE_COMMENT_CHANGE = 0;

	/**
	 * The feature id for the '<em><b>Match</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__MATCH = ExtensionsPackage.NODE_CHANGE__MATCH;

	/**
	 * The feature id for the '<em><b>Requires</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__REQUIRES = ExtensionsPackage.NODE_CHANGE__REQUIRES;

	/**
	 * The feature id for the '<em><b>Required By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__REQUIRED_BY = ExtensionsPackage.NODE_CHANGE__REQUIRED_BY;

	/**
	 * The feature id for the '<em><b>Implies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__IMPLIES = ExtensionsPackage.NODE_CHANGE__IMPLIES;

	/**
	 * The feature id for the '<em><b>Implied By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__IMPLIED_BY = ExtensionsPackage.NODE_CHANGE__IMPLIED_BY;

	/**
	 * The feature id for the '<em><b>Refines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__REFINES = ExtensionsPackage.NODE_CHANGE__REFINES;

	/**
	 * The feature id for the '<em><b>Refined By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__REFINED_BY = ExtensionsPackage.NODE_CHANGE__REFINED_BY;

	/**
	 * The feature id for the '<em><b>Prime Refining</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__PRIME_REFINING = ExtensionsPackage.NODE_CHANGE__PRIME_REFINING;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__KIND = ExtensionsPackage.NODE_CHANGE__KIND;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__SOURCE = ExtensionsPackage.NODE_CHANGE__SOURCE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__STATE = ExtensionsPackage.NODE_CHANGE__STATE;

	/**
	 * The feature id for the '<em><b>Equivalence</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__EQUIVALENCE = ExtensionsPackage.NODE_CHANGE__EQUIVALENCE;

	/**
	 * The feature id for the '<em><b>Conflict</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__CONFLICT = ExtensionsPackage.NODE_CHANGE__CONFLICT;

	/**
	 * The feature id for the '<em><b>Semantic Diff</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__SEMANTIC_DIFF = ExtensionsPackage.NODE_CHANGE__SEMANTIC_DIFF;

	/**
	 * The feature id for the '<em><b>View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE__VIEW = ExtensionsPackage.NODE_CHANGE__VIEW;

	/**
	 * The number of structural features of the '<em>Stereotype Comment Change</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_CHANGE_FEATURE_COUNT = ExtensionsPackage.NODE_CHANGE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.emf.compare.papyrus.internal.impl.StereotypeCommentLinkChangeImpl <em>Stereotype Comment Link Change</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.compare.papyrus.internal.impl.StereotypeCommentLinkChangeImpl
	 * @see org.eclipse.emf.compare.papyrus.internal.impl.PapyrusPackageImpl#getStereotypeCommentLinkChange()
	 * @generated
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE = 1;

	/**
	 * The feature id for the '<em><b>Match</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__MATCH = ExtensionsPackage.EDGE_CHANGE__MATCH;

	/**
	 * The feature id for the '<em><b>Requires</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__REQUIRES = ExtensionsPackage.EDGE_CHANGE__REQUIRES;

	/**
	 * The feature id for the '<em><b>Required By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__REQUIRED_BY = ExtensionsPackage.EDGE_CHANGE__REQUIRED_BY;

	/**
	 * The feature id for the '<em><b>Implies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__IMPLIES = ExtensionsPackage.EDGE_CHANGE__IMPLIES;

	/**
	 * The feature id for the '<em><b>Implied By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__IMPLIED_BY = ExtensionsPackage.EDGE_CHANGE__IMPLIED_BY;

	/**
	 * The feature id for the '<em><b>Refines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__REFINES = ExtensionsPackage.EDGE_CHANGE__REFINES;

	/**
	 * The feature id for the '<em><b>Refined By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__REFINED_BY = ExtensionsPackage.EDGE_CHANGE__REFINED_BY;

	/**
	 * The feature id for the '<em><b>Prime Refining</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__PRIME_REFINING = ExtensionsPackage.EDGE_CHANGE__PRIME_REFINING;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__KIND = ExtensionsPackage.EDGE_CHANGE__KIND;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__SOURCE = ExtensionsPackage.EDGE_CHANGE__SOURCE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__STATE = ExtensionsPackage.EDGE_CHANGE__STATE;

	/**
	 * The feature id for the '<em><b>Equivalence</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__EQUIVALENCE = ExtensionsPackage.EDGE_CHANGE__EQUIVALENCE;

	/**
	 * The feature id for the '<em><b>Conflict</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__CONFLICT = ExtensionsPackage.EDGE_CHANGE__CONFLICT;

	/**
	 * The feature id for the '<em><b>Semantic Diff</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__SEMANTIC_DIFF = ExtensionsPackage.EDGE_CHANGE__SEMANTIC_DIFF;

	/**
	 * The feature id for the '<em><b>View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE__VIEW = ExtensionsPackage.EDGE_CHANGE__VIEW;

	/**
	 * The number of structural features of the '<em>Stereotype Comment Link Change</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_COMMENT_LINK_CHANGE_FEATURE_COUNT = ExtensionsPackage.EDGE_CHANGE_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.emf.compare.papyrus.internal.StereotypeCommentChange <em>Stereotype Comment Change</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stereotype Comment Change</em>'.
	 * @see org.eclipse.emf.compare.papyrus.internal.StereotypeCommentChange
	 * @generated
	 */
	EClass getStereotypeCommentChange();

	/**
	 * Returns the meta object for class '{@link org.eclipse.emf.compare.papyrus.internal.StereotypeCommentLinkChange <em>Stereotype Comment Link Change</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stereotype Comment Link Change</em>'.
	 * @see org.eclipse.emf.compare.papyrus.internal.StereotypeCommentLinkChange
	 * @generated
	 */
	EClass getStereotypeCommentLinkChange();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PapyrusFactory getPapyrusFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.emf.compare.papyrus.internal.impl.StereotypeCommentChangeImpl <em>Stereotype Comment Change</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.compare.papyrus.internal.impl.StereotypeCommentChangeImpl
		 * @see org.eclipse.emf.compare.papyrus.internal.impl.PapyrusPackageImpl#getStereotypeCommentChange()
		 * @generated
		 */
		EClass STEREOTYPE_COMMENT_CHANGE = eINSTANCE.getStereotypeCommentChange();

		/**
		 * The meta object literal for the '{@link org.eclipse.emf.compare.papyrus.internal.impl.StereotypeCommentLinkChangeImpl <em>Stereotype Comment Link Change</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.compare.papyrus.internal.impl.StereotypeCommentLinkChangeImpl
		 * @see org.eclipse.emf.compare.papyrus.internal.impl.PapyrusPackageImpl#getStereotypeCommentLinkChange()
		 * @generated
		 */
		EClass STEREOTYPE_COMMENT_LINK_CHANGE = eINSTANCE.getStereotypeCommentLinkChange();

	}

} //PapyrusPackage
