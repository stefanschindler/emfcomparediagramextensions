/*******************************************************************************
* Copyright (c) 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.sysml.internal.impl;

import org.eclipse.emf.compare.sysml.internal.PortChange;
import org.eclipse.emf.compare.sysml.internal.SysMLComparePackage;

import org.eclipse.emf.compare.sysml.internal.spec.SysMLDiagramDiffSpec;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Change</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PortChangeImpl extends SysMLDiagramDiffSpec implements PortChange {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortChangeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SysMLComparePackage.Literals.PORT_CHANGE;
	}

} //PortChangeImpl
