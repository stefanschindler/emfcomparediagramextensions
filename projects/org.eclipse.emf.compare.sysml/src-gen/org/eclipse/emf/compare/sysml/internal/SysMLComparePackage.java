/*******************************************************************************
* Copyright (c) 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.sysml.internal;

import org.eclipse.emf.compare.diagram.internal.extensions.ExtensionsPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.emf.compare.sysml.internal.SysMLCompareFactory
 * @model kind="package"
 * @generated
 */
public interface SysMLComparePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "sysml"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/emf/compare/sysml/1.0"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sysmlcompare"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SysMLComparePackage eINSTANCE = org.eclipse.emf.compare.sysml.internal.impl.SysMLComparePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.emf.compare.sysml.internal.impl.SysMLDiagramDiffImpl <em>Sys ML Diagram Diff</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.compare.sysml.internal.impl.SysMLDiagramDiffImpl
	 * @see org.eclipse.emf.compare.sysml.internal.impl.SysMLComparePackageImpl#getSysMLDiagramDiff()
	 * @generated
	 */
	int SYS_ML_DIAGRAM_DIFF = 0;

	/**
	 * The feature id for the '<em><b>Match</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__MATCH = ExtensionsPackage.DIAGRAM_DIFF__MATCH;

	/**
	 * The feature id for the '<em><b>Requires</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__REQUIRES = ExtensionsPackage.DIAGRAM_DIFF__REQUIRES;

	/**
	 * The feature id for the '<em><b>Required By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__REQUIRED_BY = ExtensionsPackage.DIAGRAM_DIFF__REQUIRED_BY;

	/**
	 * The feature id for the '<em><b>Implies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__IMPLIES = ExtensionsPackage.DIAGRAM_DIFF__IMPLIES;

	/**
	 * The feature id for the '<em><b>Implied By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__IMPLIED_BY = ExtensionsPackage.DIAGRAM_DIFF__IMPLIED_BY;

	/**
	 * The feature id for the '<em><b>Refines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__REFINES = ExtensionsPackage.DIAGRAM_DIFF__REFINES;

	/**
	 * The feature id for the '<em><b>Refined By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__REFINED_BY = ExtensionsPackage.DIAGRAM_DIFF__REFINED_BY;

	/**
	 * The feature id for the '<em><b>Prime Refining</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__PRIME_REFINING = ExtensionsPackage.DIAGRAM_DIFF__PRIME_REFINING;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__KIND = ExtensionsPackage.DIAGRAM_DIFF__KIND;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__SOURCE = ExtensionsPackage.DIAGRAM_DIFF__SOURCE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__STATE = ExtensionsPackage.DIAGRAM_DIFF__STATE;

	/**
	 * The feature id for the '<em><b>Equivalence</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__EQUIVALENCE = ExtensionsPackage.DIAGRAM_DIFF__EQUIVALENCE;

	/**
	 * The feature id for the '<em><b>Conflict</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__CONFLICT = ExtensionsPackage.DIAGRAM_DIFF__CONFLICT;

	/**
	 * The feature id for the '<em><b>Semantic Diff</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__SEMANTIC_DIFF = ExtensionsPackage.DIAGRAM_DIFF__SEMANTIC_DIFF;

	/**
	 * The feature id for the '<em><b>View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF__VIEW = ExtensionsPackage.DIAGRAM_DIFF__VIEW;

	/**
	 * The number of structural features of the '<em>Sys ML Diagram Diff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYS_ML_DIAGRAM_DIFF_FEATURE_COUNT = ExtensionsPackage.DIAGRAM_DIFF_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.emf.compare.sysml.internal.impl.PortChangeImpl <em>Port Change</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.compare.sysml.internal.impl.PortChangeImpl
	 * @see org.eclipse.emf.compare.sysml.internal.impl.SysMLComparePackageImpl#getPortChange()
	 * @generated
	 */
	int PORT_CHANGE = 1;

	/**
	 * The feature id for the '<em><b>Match</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__MATCH = SYS_ML_DIAGRAM_DIFF__MATCH;

	/**
	 * The feature id for the '<em><b>Requires</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__REQUIRES = SYS_ML_DIAGRAM_DIFF__REQUIRES;

	/**
	 * The feature id for the '<em><b>Required By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__REQUIRED_BY = SYS_ML_DIAGRAM_DIFF__REQUIRED_BY;

	/**
	 * The feature id for the '<em><b>Implies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__IMPLIES = SYS_ML_DIAGRAM_DIFF__IMPLIES;

	/**
	 * The feature id for the '<em><b>Implied By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__IMPLIED_BY = SYS_ML_DIAGRAM_DIFF__IMPLIED_BY;

	/**
	 * The feature id for the '<em><b>Refines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__REFINES = SYS_ML_DIAGRAM_DIFF__REFINES;

	/**
	 * The feature id for the '<em><b>Refined By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__REFINED_BY = SYS_ML_DIAGRAM_DIFF__REFINED_BY;

	/**
	 * The feature id for the '<em><b>Prime Refining</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__PRIME_REFINING = SYS_ML_DIAGRAM_DIFF__PRIME_REFINING;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__KIND = SYS_ML_DIAGRAM_DIFF__KIND;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__SOURCE = SYS_ML_DIAGRAM_DIFF__SOURCE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__STATE = SYS_ML_DIAGRAM_DIFF__STATE;

	/**
	 * The feature id for the '<em><b>Equivalence</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__EQUIVALENCE = SYS_ML_DIAGRAM_DIFF__EQUIVALENCE;

	/**
	 * The feature id for the '<em><b>Conflict</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__CONFLICT = SYS_ML_DIAGRAM_DIFF__CONFLICT;

	/**
	 * The feature id for the '<em><b>Semantic Diff</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__SEMANTIC_DIFF = SYS_ML_DIAGRAM_DIFF__SEMANTIC_DIFF;

	/**
	 * The feature id for the '<em><b>View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE__VIEW = SYS_ML_DIAGRAM_DIFF__VIEW;

	/**
	 * The number of structural features of the '<em>Port Change</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CHANGE_FEATURE_COUNT = SYS_ML_DIAGRAM_DIFF_FEATURE_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.eclipse.emf.compare.sysml.internal.SysMLDiagramDiff <em>Sys ML Diagram Diff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sys ML Diagram Diff</em>'.
	 * @see org.eclipse.emf.compare.sysml.internal.SysMLDiagramDiff
	 * @generated
	 */
	EClass getSysMLDiagramDiff();

	/**
	 * Returns the meta object for class '{@link org.eclipse.emf.compare.sysml.internal.PortChange <em>Port Change</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Change</em>'.
	 * @see org.eclipse.emf.compare.sysml.internal.PortChange
	 * @generated
	 */
	EClass getPortChange();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SysMLCompareFactory getSysMLCompareFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.emf.compare.sysml.internal.impl.SysMLDiagramDiffImpl <em>Sys ML Diagram Diff</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.compare.sysml.internal.impl.SysMLDiagramDiffImpl
		 * @see org.eclipse.emf.compare.sysml.internal.impl.SysMLComparePackageImpl#getSysMLDiagramDiff()
		 * @generated
		 */
		EClass SYS_ML_DIAGRAM_DIFF = eINSTANCE.getSysMLDiagramDiff();

		/**
		 * The meta object literal for the '{@link org.eclipse.emf.compare.sysml.internal.impl.PortChangeImpl <em>Port Change</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.compare.sysml.internal.impl.PortChangeImpl
		 * @see org.eclipse.emf.compare.sysml.internal.impl.SysMLComparePackageImpl#getPortChange()
		 * @generated
		 */
		EClass PORT_CHANGE = eINSTANCE.getPortChange();

	}

} //SysMLComparePackage
