/*******************************************************************************
* Copyright (c) 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.sysml.internal.impl;

import org.eclipse.emf.compare.diagram.internal.extensions.impl.DiagramDiffImpl;

import org.eclipse.emf.compare.sysml.internal.SysMLComparePackage;
import org.eclipse.emf.compare.sysml.internal.SysMLDiagramDiff;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sys ML Diagram Diff</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class SysMLDiagramDiffImpl extends DiagramDiffImpl implements SysMLDiagramDiff {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SysMLDiagramDiffImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SysMLComparePackage.Literals.SYS_ML_DIAGRAM_DIFF;
	}

} //SysMLDiagramDiffImpl
