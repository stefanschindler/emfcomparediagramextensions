/*******************************************************************************
* Copyright (c) 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.sysml.internal.impl;

import org.eclipse.emf.compare.diagram.internal.extensions.ExtensionsPackage;

import org.eclipse.emf.compare.sysml.internal.PortChange;
import org.eclipse.emf.compare.sysml.internal.SysMLCompareFactory;
import org.eclipse.emf.compare.sysml.internal.SysMLComparePackage;
import org.eclipse.emf.compare.sysml.internal.SysMLDiagramDiff;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SysMLComparePackageImpl extends EPackageImpl implements SysMLComparePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sysMLDiagramDiffEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portChangeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.emf.compare.sysml.internal.SysMLComparePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SysMLComparePackageImpl() {
		super(eNS_URI, SysMLCompareFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SysMLComparePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SysMLComparePackage init() {
		if (isInited)
			return (SysMLComparePackage) EPackage.Registry.INSTANCE.getEPackage(SysMLComparePackage.eNS_URI);

		// Obtain or create and register package
		SysMLComparePackageImpl theSysMLComparePackage = (SysMLComparePackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof SysMLComparePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new SysMLComparePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ExtensionsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSysMLComparePackage.createPackageContents();

		// Initialize created meta-data
		theSysMLComparePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSysMLComparePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SysMLComparePackage.eNS_URI, theSysMLComparePackage);
		return theSysMLComparePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSysMLDiagramDiff() {
		return sysMLDiagramDiffEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortChange() {
		return portChangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SysMLCompareFactory getSysMLCompareFactory() {
		return (SysMLCompareFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		sysMLDiagramDiffEClass = createEClass(SYS_ML_DIAGRAM_DIFF);

		portChangeEClass = createEClass(PORT_CHANGE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ExtensionsPackage theExtensionsPackage = (ExtensionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ExtensionsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		sysMLDiagramDiffEClass.getESuperTypes().add(theExtensionsPackage.getDiagramDiff());
		portChangeEClass.getESuperTypes().add(this.getSysMLDiagramDiff());

		// Initialize classes and features; add operations and parameters
		initEClass(sysMLDiagramDiffEClass, SysMLDiagramDiff.class, "SysMLDiagramDiff", IS_ABSTRACT, !IS_INTERFACE, //$NON-NLS-1$
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(portChangeEClass, PortChange.class, "PortChange", !IS_ABSTRACT, !IS_INTERFACE, //$NON-NLS-1$
				IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //SysMLComparePackageImpl
