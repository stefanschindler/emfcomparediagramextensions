/*******************************************************************************
* Copyright (c) 2013, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.sysml.internal.spec;

import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.sysml.internal.PortChange;

public interface PortChangeSpec extends PortChange {

	public void setPrimeRefining(Diff diff);
}
