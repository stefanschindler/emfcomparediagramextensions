/*******************************************************************************
* Copyright (c) 2013, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.sysml.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.internal.postprocessor.factories.IChangeFactory;

public class SysMLExtensionFactoryRegistry {

	
	/**
	 * No need to instantiate this class.
	 */
	private SysMLExtensionFactoryRegistry() {
		
	}
	
	public static Map<Class<? extends Diff>, IChangeFactory> createExtensionFactories() {
		
		Map<Class<? extends Diff>, IChangeFactory> factoryMap = new HashMap<Class<? extends Diff>, IChangeFactory>();
		
		List<IChangeFactory> factories = new ArrayList<IChangeFactory>();
		
		factories.add(new PortChangeFactory());
				
		for(IChangeFactory factory : factories) {
			
			factoryMap.put(factory.getExtensionKind(), factory);
			
		}
		
		return Collections.unmodifiableMap(factoryMap);
	}
}
