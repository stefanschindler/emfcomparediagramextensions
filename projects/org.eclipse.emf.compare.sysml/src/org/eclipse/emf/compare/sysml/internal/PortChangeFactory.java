/*******************************************************************************
* Copyright (c) 2013, 2015, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.sysml.internal;

import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.DifferenceKind;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.diagram.internal.extensions.DiagramDiff;
import org.eclipse.emf.compare.diagram.internal.extensions.NodeChange;
import org.eclipse.emf.compare.diagram.internal.factories.extensions.NodeChangeFactory;
import org.eclipse.emf.compare.sysml.internal.spec.PortChangeSpec;
import org.eclipse.emf.compare.utils.EMFComparePredicates;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Shape;
import org.eclipse.gmf.runtime.notation.View;

import com.google.common.collect.Collections2;

public class PortChangeFactory extends NodeChangeFactory {

	private static final String TYPE_PORT = "shape_uml_port_as_affixed";
	private static final String TYPE_STEREOTYPE_LABEL ="StereotypeLabel";
	private static final String TYPE_STEREOTYPE_BRACE ="StereotypeBrace";
	private static final String TYPE_STEREOTYPE_COMPARTMENT ="StereotypeCompartment";
	private static final String TYPE_COMPARTMENT_SHAPE_DISPLAY ="compartment_shape_display";
	
	public PortChangeFactory() {
		
	}
	
	@Override
	public Class<? extends Diff> getExtensionKind() {
		return PortChange.class;
	}
	
	@Override
	public DiagramDiff createExtension() {
		
		return SysMLCompareFactory.eINSTANCE.createPortChange();
	}
	
	@Override
	protected boolean isRelatedToAnExtensionAdd(ReferenceChange input) {
		
		boolean ret = isRelatedToAnExtensionKind(input, DifferenceKind.ADD);		
				
		return ret;
	}
	
	@Override
	protected boolean isRelatedToAnExtensionDelete(ReferenceChange input) {
		
		boolean ret = isRelatedToAnExtensionKind(input, DifferenceKind.DELETE);		
		
		return ret;
	}
	
	@Override
	protected boolean isRelatedToAnExtensionMove(ReferenceChange input) {
		
		boolean ret = isRelatedToAnExtensionKind(input, DifferenceKind.MOVE);		
		
		return ret;
	}
	
	protected boolean isRelatedToAnExtensionKind(ReferenceChange input, DifferenceKind kind) {
		
		boolean ret = false;
		
		if(input.getKind() == kind) {
			
			ret = referesToPort(input);
			
		}
		
		return ret;
		
	}

	protected boolean referesToPort(ReferenceChange input) {
		
		boolean ret = false;
		
		EObject value = input.getValue();
		
		if(value instanceof Shape) {
			
			ret = ((Shape) value).getType() == TYPE_PORT;
			
		}
		
		return ret;
	}
	
	@Override
	public void setRefiningChanges(Diff extension, DifferenceKind extensionKind, Diff refiningDiff) {

		if (refiningDiff.getSource() == extension.getSource()) {

			if (extensionKind == DifferenceKind.DELETE) {

				extension.getRefinedBy().add(refiningDiff.getRefines().get(0));
				extension.getRefinedBy().addAll(refiningDiff.getRefines().get(0).getRefinedBy());
				extension.getRefinedBy().remove(refiningDiff.getRefines().get(0).getPrimeRefining());

				setPrimeRefining((PortChangeSpec) extension, (ReferenceChange) refiningDiff);

			} else {

				Match m = refiningDiff.getMatch();

				for (Diff diff : m.getDifferences()) {

					if (diff instanceof NodeChange) {

						EObject eo = ((NodeChange) diff).getView();

						if (eo != null) {

							String type = ((View) eo).getType();
							if (type == TYPE_STEREOTYPE_LABEL || type == TYPE_STEREOTYPE_BRACE || type == TYPE_STEREOTYPE_COMPARTMENT) {

								extension.getRefinedBy().add(diff);

							} else if (type == TYPE_PORT) {

								extension.getRefinedBy().add(diff);
								extension.getRefinedBy().addAll(diff.getRefinedBy());
								extension.getRefinedBy().remove(diff.getPrimeRefining());
							}

							if (eo.equals(((ReferenceChange) refiningDiff).getValue())) {

								setPrimeRefining((PortChangeSpec) extension, (ReferenceChange) refiningDiff);
							}
						}
					} else if (diff instanceof ReferenceChange && diff.getRefines().isEmpty()) {

						EObject eo = ((ReferenceChange) diff).getValue();

						if (eo != null && eo instanceof View && ((View) eo).getType() == TYPE_COMPARTMENT_SHAPE_DISPLAY) {

							extension.getRefinedBy().add(diff);

						}

					}
				}
			}
		}
	}
	
	public void setPrimeRefining(PortChangeSpec extension, ReferenceChange refiningDiff) {
		extension.setPrimeRefining(refiningDiff);
	}
	
	@Override
	public void fillRequiredDifferences(Comparison comparison, Diff extension) {
		fillRequiredDifferencesForMacroToMacro(extension);
		fillRequiredDifferencesForUnitToMacro(extension);
	}
	
	private void fillRequiredDifferencesForMacroToMacro(Diff macro) {
		
		Set<Diff> requiredExtensions = new LinkedHashSet<Diff>();
		Set<Diff> requiringExtensions = new LinkedHashSet<Diff>();
		for (Diff refiningDiff : macro.getRefinedBy()) {
			
			for (Diff unit : refiningDiff.getRequires()) {
				
				requiredExtensions.addAll(unit.getRefines());
			}
			
			for (Diff unit : refiningDiff.getRequiredBy()) {
				
				requiringExtensions.addAll(unit.getRequiredBy());
			}
		}
		
		requiredExtensions.remove(macro);
		requiringExtensions.remove(macro);

		macro.getRequires().addAll(
				Collections2.filter(requiredExtensions, EMFComparePredicates.fromSide(macro.getSource())));
		macro.getRequiredBy().addAll(
				Collections2.filter(requiringExtensions, EMFComparePredicates.fromSide(macro.getSource())));
	}
	
	private void fillRequiredDifferencesForUnitToMacro(Diff macro) {
		Set<Diff> requiredExtensions = new LinkedHashSet<Diff>();
		Set<Diff> requiringExtensions = new LinkedHashSet<Diff>();
		for (Diff refiningDiff : macro.getRefinedBy()) {
			for (Diff unit : refiningDiff.getRequires()) {
				if (unit.getRefines().isEmpty()) {
					requiredExtensions.add(unit);
				}
			}
			for (Diff unit : refiningDiff.getRequiredBy()) {
				if (unit.getRefines().isEmpty()) {
					requiringExtensions.add(unit);
				}
			}
		}
		
		requiredExtensions.remove(macro);
		requiringExtensions.remove(macro);
		
		macro.getRequires().addAll(
				Collections2.filter(requiredExtensions, EMFComparePredicates.fromSide(macro.getSource())));
		macro.getRequiredBy().addAll(
				Collections2.filter(requiringExtensions, EMFComparePredicates.fromSide(macro.getSource())));
	}

}
