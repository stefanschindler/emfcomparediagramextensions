/*******************************************************************************
* Copyright (c) 2013, 2015, 2018 Obeo and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* 
* Contributors:
*     Obeo - initial API and implementation
*     Stefan Schindler - extension to handle unexpected diagram differences (UDDs)
*******************************************************************************/

package org.eclipse.emf.compare.sysml.internal.spec;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.ComparePackage;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.sysml.internal.SysMLDiagramDiff;
import org.eclipse.emf.compare.sysml.internal.impl.SysMLDiagramDiffImpl;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

public class SysMLDiagramDiffSpec extends SysMLDiagramDiffImpl implements PortChangeSpec, SysMLDiagramDiff{

	protected SysMLDiagramDiffSpec() {
		super();
	}

		
	@Override
	public void setPrimeRefining(Diff diff) {
		primeRefining = diff;
	}
	
	//Methods from org.eclipse.emf.compare.diagram.internal.extensions.spec.DiagramDiffSpec
	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.impl.DiffImpl#basicGetMatch()
	 */
	@Override
	public Match basicGetMatch() {
		if (eContainer() instanceof Match) {
			return (Match)eContainer();
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.impl.DiffImpl#setMatch(org.eclipse.emf.compare.Match)
	 */
	@Override
	public void setMatch(Match newMatch) {
		Match oldMatch = basicGetMatch();
		if (newMatch != null) {
			EList<Diff> differences = newMatch.getDifferences();
			differences.add(this);
			eNotify(new ENotificationImpl(this, Notification.SET, ComparePackage.DIFF__MATCH, oldMatch,
					newMatch));
		} else if (eContainer() instanceof Match) {
			EList<Diff> differences = ((Match)eContainer()).getDifferences();
			differences.remove(this);
			eNotify(new ENotificationImpl(this, Notification.UNSET, ComparePackage.DIFF__MATCH, oldMatch,
					newMatch));

		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.diagram.internal.extensions.impl.DiagramDiffImpl#getPrimeRefining()
	 */
	@Override
	public Diff getPrimeRefining() {
		if (primeRefining == null) {
			for (Diff refBy : this.getRefinedBy()) {
				if (refBy instanceof ReferenceChange) {
					ReferenceChange rc = (ReferenceChange)refBy;
					if (this.getView() == rc.getValue()) {
						primeRefining = rc;
						break;
					}
				} else if (refBy instanceof AttributeChange) {
					AttributeChange ac = (AttributeChange)refBy;
					if (this.getView() == ac.getValue()) {
						primeRefining = ac;
						break;
					}
				}
			}
		}
		return primeRefining;
	}
	
}
